using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace AspChat.Hubs
{
    public class ChatHub : Hub
    {
        public async Task Send(string username, string message)
        {
            await Clients.All.SendAsync("Send", username, message);
        }
    }    
}